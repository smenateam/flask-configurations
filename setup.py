from os.path import join, dirname

from setuptools import setup


setup(
    name='flask_configurations',
    version=0.1,
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    author='Rebranch',
    url='https://bitbucket.org/rebranch/rebranch-flask-configurations',
)