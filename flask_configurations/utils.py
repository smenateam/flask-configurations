from os import environ

try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse


class EnvironmentBasedValue(object):
    name = None
    value = None

    def __init__(self, name, default=None):
        self.name = name
        self.value = environ.get(name, default)

    def __get__(self, instance, owner):
        return self.value


class DockerDatabaseSettings(object):
    database_url = EnvironmentBasedValue(u'DATABASE_URL')

    def __get__(self, instance, owner):
        database_config = {
            u'ENGINE': u'',
            u'NAME': u'',
            u'USER': u'',
            u'PASSWORD': u'',
            u'HOST': u'',
            u'PORT': u'',
        }
        if self.database_url:
            url_parsed = urlparse(self.database_url)
            database_config[u'ENGINE'] = url_parsed.scheme
            database_config[u'NAME'] = url_parsed.path[1:]
            database_config[u'USER'] = url_parsed.username
            database_config[u'PASSWORD'] = url_parsed.password
            database_config[u'HOST'] = url_parsed.hostname
            database_config[u'PORT'] = url_parsed.port

        return database_config