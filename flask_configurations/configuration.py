import os
import inspect
from .base import FlaskSettings

try:
    import settings as settings_module
except ImportError:
    settings = FlaskSettings()
else:
    settings = None
    configuration = os.environ.get(u'FLASK_CONFIGURATION', u'Development')
    for name, obj in inspect.getmembers(settings_module):
        if inspect.isclass(obj) and name == configuration and issubclass(obj, FlaskSettings):
            settings = obj()
            break
    assert settings is not None, u'Configuration class with name "{0}" not found'.format(configuration)